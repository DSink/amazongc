# Details

This is a **__jenky script__** that will buy amazon gift card reloads. 
It currently requests your credentials because I don't want to be
responsible for storing those for you. If you go crazy with this 
don't be surprised if Amazon starts throwing captcha-like roadblocks in your
way. 

# Setup

## Option 1 - Run natively 

1. Pull repository 
```
git clone DSink/amazongc
cd amazongc
```

2. Install dependencies
```
pip install -r requirements.txt
```
Install selenium driver of your choice [(Chrome)](https://sites.google.com/a/chromium.org/chromedriver/getting-started) and add the path to that driver to amazongc.py file. (e.g. driver_path = 'C:\Python27\selenium\webdriver\chrome\chromedriver').

3. Run
```
python amazongc.py
```
