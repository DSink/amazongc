from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
import time
import getpass

#Set Browser Driver Here
driver_path = 'C:\Python27\selenium\webdriver\chrome\chromedriver'

#Get credentials
eAddr = raw_input('Enter Amazon email: ')
pwd = getpass.getpass('Enter Amazon password: ')
cc = raw_input('Enter debit card #: ')

#Set up webdriver instance
driver = webdriver.Chrome(driver_path)
wait = WebDriverWait(driver, 10)
driver.get('https://www.amazon.com/asv/reload/')

#Get around CAPTCHA if you get blacklisted
try:
	driver.find_element_by_id('form-submit-button').click()
except NoSuchElementException:
	time.sleep(1)
	driver.get('https://www.amazon.com/asv/reload/')
	driver.find_element_by_id('form-submit-button').click()

#Lo
driver.find_element_by_id('ap_email').send_keys(eAddr)
time.sleep(1)	#Let reload balance appear
driver.find_element_by_id('ap_password').send_keys(pwd)
time.sleep(1)	#Let reload balance appear
driver.find_element_by_id('signInSubmit').click()
time.sleep(1)	#Let reload balance appear


for i in range(10):

	if driver.title != 'Reload Your Balance':
		driver.get('https://www.amazon.com/asv/reload/')

	#Balance page
	driver.find_element_by_id('asv-manual-reload-amount').send_keys('0.50')
	#TODO: Better sleep (Sometimes got "no card with XXXX" error)
	time.sleep(1)
	cardElement = driver.find_element_by_xpath("//span[contains(number(),"+ cc[-4:] +")]")
	driver.execute_script("arguments[0].scrollIntoView();", cardElement)
	cardElement.click()
		
	try: 
		driver.find_element_by_name('addCardNumber').send_keys(cc)
		driver.find_element_by_xpath("//button[contains(text(), 'Confirm Card')]").click()
	except:
		pass
		
	payButton = driver.find_element_by_xpath("//button[contains(text(), 'Reload')]")
	driver.execute_script("arguments[0].scrollIntoView();", payButton)
	#TODO: Better sleep (Sometimes button takes a second to click)
	time.sleep(2)
	#payButton.click()
	
	#Loop sleep time
	time.sleep(10)
	
driver.close()